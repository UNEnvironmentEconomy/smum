#### CHANGE VALUES #### --------------------------------V
#HOME_PATH=/home/esteban/workspace/mva/mva_deploy/
#HOME_FOLDER_MAIN=fof/
#HOME_FOLDER_LOCAL=fof_local/
#HOME_FOLDER_LOCAL_BUK=fof_local_buk/
#HOME_FOLDER_CLOUD=fof_cloud/
#### CHANGE VALUES #### --------------------------------A

.PHONY: build
build:
	docker build . -t smum

.PHONY: run
run:
	docker run smum

.PHONY: shell
shell:
	docker run -it -p 8080:8080 smum
