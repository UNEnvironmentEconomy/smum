#!/usr/bin/env python
# -*- coding:utf -*-
"""
#Created by Esteban.

mié 18 sep 2019 23:44:50 -05

"""

import pandas as pd

# G var
CODES = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
         'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U']
SKIP = ['Total']
BREAKS = ['35', '36', '49', '64', '68', '69', '85']


def _aggregate_codes(dat, clean_NACE):
    new_codes = list()
    e = 0
    for i in dat.columns:
        if i not in SKIP:
            num_code = i.split(' ')[0]
            if num_code not in CODES and num_code not in BREAKS:
                num_code = CODES[e] + num_code
            elif num_code in BREAKS:
                e += 1
                num_code = CODES[e] + num_code
            else:
                e += 2
            new_codes.append(num_code)

    skip = SKIP.copy()
    skip.extend(new_codes)
    dat.columns = skip
    for code in clean_NACE:
        if code not in dat.columns:
            agg_range = code.split('_')
            new_dat = dat.loc[:, agg_range[0]:agg_range[1]].sum(axis=1)
            dat.loc[:, code] = new_dat
            for col in dat.loc[:, agg_range[0]:agg_range[1]].columns:
                del dat[col]
    skip = SKIP.copy()
    skip.extend(sorted([c for c in dat.columns if c not in SKIP]))
    dat = dat.reindex(skip, axis=1)
    return(dat)


def _get_data(data_file, source, target):
    dat = pd.read_excel(data_file,
                        header=4, skipfooter=1, index_col=0)
    dat = dat.replace('-', 0)
    dat.index = [source, target]
    return(dat)


def _get_data_in(data_file, source, target):
    dat = pd.read_excel(data_file, index_col=0)
    dat.index = [source, target]
    return(dat)


def get_factor(IOtab, data_file, source='BR', target='RE', mod='year'):

    clean_NACE = IOtab.tables[source].df.index.to_list()
    if isinstance(data_file, pd.DataFrame):
        dat = data_file
    else:
        if "BR" in source:
            dat = _get_data(data_file, source, target)
            dat = _aggregate_codes(dat, clean_NACE)
        elif "IN" in source:
            dat = _get_data_in(data_file, source, target)
        else:
            dat = _get_data2(data_file, source, target)

    factor = IOtab.tables[source].df.sum().div(
        dat.loc[source, [c for c in dat.columns if c not in SKIP]])
    if mod == 'downscale':
        factor = factor.mul(
            dat.loc[target, [c for c in dat.columns if c not in SKIP]])
    factor.loc[factor.isna()] = 0
    return(factor)


def _get_data2(data_file, source, target):
    dat = pd.read_excel(data_file, sheet_name='Scaling Factors',
                        header=1, index_col=0, usecols=[0, 1, 2],
                        skipfooter=1)
    dat = dat.replace('-', 0)
    dat = dat.T
    dat.index = [source, target]
    return(dat)
