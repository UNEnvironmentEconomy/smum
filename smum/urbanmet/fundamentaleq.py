"""
#Created by Michelle.

Mon 16 April 2018 02:40 PM CEST

Set of fundamental equations when describing the urban metabolism of a city.

Please refer to documentation and cited literature for theoretical background
to the equations listed here.
"""


########
# Buildings Equations
########

#I_{E,heating} = \sum_{building-type} HDD * i_{E,heating} * P * f

#I_{E,cooling} = \sum_{building-type} CDD * i_{E,cooling} * P * f * cp

def _get_HVAC(i_building, HCDD, Pop, floor_person, cp=1):
    """Building HVAC."""

    if isinstance(i_building, float) or isinstance(i_building, int):
        i_building = [i_building]

    if isinstance(Pop, float) or isinstance(Pop, int):
        Pop = [Pop]

    if len(Pop) != len(i_building):
        raise Error("len of Pop = {}, len of i_building = {}".format(len(Pop), len(i_building)))

    I_heatcool = 0
    for intensity, pop in zip(i_building, Pop):
        I_heatcool += intensity * HCDD * pop * floor_person * cp
    return I_heatcool


def buildings_I(i_building, CDD, HDD, Pop, floor_person, cp, I_light=0, I_dhw=0):
    """Calculates total energy demand across building typeself.

    :math:`I_{E,buildings} = I_{E,heating} + I_{E,cooling} + I_{E,light-and-appl.} + I_{E,water-heating}`

    see :ref:`energy_flow`

    Args:
        i_building (list): heating intensity per building types
        CDD (float): cooling degree days
        HDD (float): heating degree days
        Pop (list): population
        floor_person (float): floor area per person
        cp (float):
        I_light (float): energy demand for lighting
        I_dhw (float): energy demand for domestic hot water

    """
    #Below are already implementation equations. Question if to separate these out
    I_heating = _get_HVAC(i_building, HDD, Pop, floor_person)
    I_cooling = _get_HVAC(i_building, CDD, Pop, floor_person, cp=cp)
    I_building = I_heating + I_cooling + I_light + I_dhw
    return I_building



########
# Transport Equations
########

def _get_passengerTransport(mode, rho_Pop, Pop, Rho_infrastructure, h, epsilon):

    if isinstance(mode, float) or isinstance(mode, int):
        mode = [mode]

    if isinstance(Pop, float) or isinstance(Pop, int):
        Pop = [Pop]

    if len(Pop) != len(mode):
        raise Error("len of Pop = {}, len of mode = {}".format(len(Pop), len(mode)))


        I_passangerTRA = 0
        for m, pop, rho_infrastructure in zip(mode, Pop, Rho_infrastructure):
            print(m)
            I_passangerTRA += 1/rho_Pop * pop * rho_infrastructure * h * epsilon

        return I_passangerTRA

# To consider including equations for other transportation categories
#def _get_marineTransport(a, b, c)
#Interesting for cities with (major) port

#def _get_aviationTransport(d, e, f)
#Interesting for cities with (major) airport


def transport_I(mode, rho_Pop, Pop, Rho_infrastructure, h, epsilon, I_freight = 0, I_aviation = 0, I_marine = 0):
    #I_{E,transport} = I_{E,passenger} + I_{E,freight} + I_{E,aviation} + I_{E,marine}
    """Calculates total transport energy use per transportation subsector and according to passenger mode.

    Args:
        mode (list): passenger mode types
        rho_Pop (float): average population density
        Pop (float): population
        Rho_infrastructure (list): density of transportation infrastructure per mode
        h (float): utilization of infrastructure
        eplison (float): fuel efficiency
        I_freight (float): energy use of freight transport systems
        I_aviation (float): energy use of freight transport systems
        I_marine (float): energy use of freight transport systems

    """
    #Below are already implementation equations; but make it for transport still
    I_passenger = _get_passengerTransport(mode, rho_Pop, Pop, Rho_infrastructure, h, epsilon)
    I_transport = I_passenger + I_freight + I_aviation + I_marine
    return I_transport

########
# Water Equations
########
