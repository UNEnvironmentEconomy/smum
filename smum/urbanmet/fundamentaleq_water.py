"""
# Created by Michelle.

Mon 16 April 2018 02:40 PM CEST

Set of fundamental equations when describing the urban metabolism of a city.

Please refer to documentation and cited literature for theoretical background
to the equations listed here.
"""


########
# Buildings Equations
########


def _get_HVAC(i_building, HCDD, Pop, floor_person, cop=1):
    """Building HVAC.

    **Heating**

    :math:`I_{E,heating} = \sum_{building-type} HDD * i_{E,heating} * P * f`

    **Cooling**

    :math:`I_{E,cooling} = \sum_{building-type} CDD * i_{E,cooling} * P * f * cp`
    """

    if isinstance(i_building, float) or isinstance(i_building, int):
        i_building = [i_building]

    if isinstance(Pop, float) or isinstance(Pop, int):
        Pop = [Pop]

    if len(Pop) != len(i_building):
        raise Exception("len of Pop = {}, len of i_building = {} should be equal.".format(len(Pop), len(i_building)))

    I_heatcool = 0
    for intensity, pop in zip(i_building, Pop):
        I_heatcool += intensity * HCDD * pop * floor_person * cop
    return I_heatcool


def buildings_I(i_building, CDD, HDD, Pop, floor_person, cop, I_light=0, I_dhw=0):
    """Calculates total energy demand across building types.
    :math:`I_{E,buildings} = I_{E,heating} + I_{E,cooling} + I_{E,light-and-appl.} + I_{E,water-heating}`


    see :ref:`energy_flow`

    Args:
        i_building (list): heating intensity per building types
        CDD (float): cooling degree days
        HDD (float): heating degree days
        Pop (list): population
        floor_person (float): floor area per person
        cop (float): Coefficient of performance
        I_light (float): energy demand for lighting
        I_dhw (float): energy demand for domestic hot water

    """
    I_heating = _get_HVAC(i_building, HDD, Pop, floor_person)
    I_cooling = _get_HVAC(i_building, CDD, Pop, floor_person, cop=cop)
    I_building = I_heating + I_cooling + I_light + I_dhw
    return I_building


########
# Transport Equations
########


def _get_passengerTransport(mode, rho_pop, pop, rho_infrastructure, h, epsilon):

    if isinstance(mode, float) or isinstance(mode, int):
        mode = [mode]

    if isinstance(pop, float) or isinstance(pop, int):
        pop = [pop]

    if len(pop) != len(mode):
        raise Exception("len of Pop = {}, len of mode = {} should be equal".format(len(pop), len(mode)))

        I_passangerTRA = 0
        for m, pop, rho_inf in zip(mode, pop, rho_infrastructure):
            I_passangerTRA += 1/rho_pop * pop * rho_inf * h * epsilon

        return I_passangerTRA

#TODO: consider including equations for other transportation categories
#def _get_marineTransport(a, b, c)
#Interesting for cities with (major) port

#def _get_aviationTransport(d, e, f)
#Interesting for cities with (major) airport


def transport_I(mode, rho_Pop, pop, rho_infrastructure, h, epsilon, I_freight=0, I_aviation=0, I_marine=0):
    """Calculates total transport energy use per transportation subsector and according to passenger mode.

    :math:`I_{E,transport} = I_{E,passenger} + I_{E,freight} + I_{E,aviation} + I_{E,marine}`

    :math:`I_{E,passenger} = \sum_{mode} \frac{1}{P_p} * P * \rho_i * h * \varepsilon`

    Args:
        mode (list): passenger mode types
        rho_Pop (float): average population density
        pop (float): population
        rho_infrastructure (list): density of transportation infrastructure per mode type
        h (float): utilization of infrastructure
        eplison (float): fuel efficiency
        I_freight (float): energy use of freight transport systems
        I_aviation (float): energy use of freight transport systems
        I_marine (float): energy use of freight transport systems

    """
    I_passenger = _get_passengerTransport(mode, rho_Pop, pop, rho_infrastructure, h, epsilon)
    I_transport = I_passenger + I_freight + I_aviation + I_marine
    return I_transport

########
# Water Equations
########


def water():
    """
    :math:`Q_W = Q^{hh}_{W,D} + Q^{nr}_{W,D}`
    Returns:

    """
