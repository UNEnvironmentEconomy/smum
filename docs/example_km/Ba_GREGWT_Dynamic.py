#!/usr/bin/env python
# coding: utf-8

# ## Lalitpur. Step 2.a Dynamic Sampling Model  and GREGWT

# In[23]:


import datetime; print(datetime.datetime.now())


# **Notebook abstract**
# 
# This notebook shows the main sampling and reweighting algorithm.

# ### Import libraries

# In[2]:


#from smum.microsim.run import run_calibrated_model
#from smum.microsim.table import TableModel


# In[3]:


import sys
sys.path.append('/home/esteban/workspace/smum/smum/')
from microsim.run import run_calibrated_model
from microsim.table import TableModel


# ### Global variables

# In[4]:


iterations = 1000
benchmark_year = 2016
census_file = 'data/benchmarks_year_bias.csv'
typ = 'resampled'
model_name = 'Lalitpur_Electricity_wbias_projected_dynamic_{}'.format(typ)
verbose = False
#The number of chains to run in parallel. 
njobs = 4


# ### Define Table model

# In[5]:


tm = TableModel(census_file=census_file, verbose=verbose)


# #### Income model

# In[6]:


tm.add_model('data/table_inc.csv', 'Income')
tm.update_dynamic_model('Income', specific_col='Education')
tm.update_dynamic_model('Income',
                        specific_col='FamilySize',
                        specific_col_as='Size',
                        val = 'mu', compute_average=0)
tm.update_dynamic_model('Income',
                        specific_col = 'Age',
                        val = 'mu', compute_average=0)


# In[7]:


formula_inc = "i_Intercept+"+"+".join(
    ["c_{0} * {0}".format(e.values) for e in tm.models['Income'].coords['dependent_var'] if\
        (e != 'i_Intercept')])
tm.add_formula(formula_inc, 'Income')


# In[8]:


tm.print_formula('Income')


# #### Electricity model

# In[9]:


tm.add_model('data/table_elec.csv',  'Electricity', reference_cat=['yes'])
tm.update_dynamic_model('Electricity', specific_col='Income', val='mu', compute_average=False)


# In[10]:


#tm.models['Electricity'][2016]


# In[11]:


formula_elec = "e_Intercept+"+"+".join(
    ["c_{0} * {0}".format(e.values) for e in tm.models['Electricity'].coords['dependent_var'] if\
        (e != 'e_Intercept') &\
        (e != 'e_Income') &\
        (e != 'e_Urban')
    ])
formula_elec += '+c_e_Urban * i_Urbanity'
formula_elec += '+c_e_{0} * {0}'.format('Income')


# In[12]:


tm.add_formula(formula_elec, 'Electricity')


# In[13]:


tm.print_formula('Electricity')


# #### Make model and save it to excel

# In[14]:


table_model = tm.make_model()


# In[15]:


#a = tm.models['Electricity']


# In[16]:


#[i for i in a.keys()]


# In[17]:


#var = 'Electricity'
#year = 2010
#df = tm.models[var][year].to_dataframe().unstack()
#df.columns = df.columns.droplevel(0)


# In[18]:


#df


# In[19]:


#tm.models.keys()


# In[20]:


#tm.to_excel(raw=True)


# ### Define model variables

# In[21]:


labels = ['age_0_18', 'age_19_25', 'age_26_35',
          'age_36_45', 'age_46_55', 'age_56_65',
          'age_66_75', 'age_76_85', 'age_86_100']
cut = [0, 19, 26, 36, 46, 56, 66, 76, 86, 101]
to_cat = {'i_Age':[cut, labels]}
drop_col_survey = ['e_Income', 'e_Urban', 'w_Total_Family_Income', 'w_Education']


# In[22]:


fw = run_calibrated_model(
    table_model,
    project = typ,
    njobs = njobs,
    #rep = {'FamilySize': ['Size']},
    #rep={'urb': ['urban', 'urbanity']},
    census_file = census_file,
    year = benchmark_year,
    population_size = False,
    name = '{}_{}'.format(model_name, iterations),
    to_cat = to_cat,
    iterations = iterations,
    verbose = verbose,
    drop_col_survey = drop_col_survey)

